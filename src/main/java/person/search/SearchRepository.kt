package person.search

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import person.types.Person
import java.util.concurrent.TimeUnit

class SearchRepository(private val searchView: SearchView, private val personBackend: PersonBackend) {


    fun search(): Observable<List<Person>> {

        /**
         * Implement the search call according the following criteria:
         * - The search query string must contain at least 3 characters
         * - To save backend traffic: only search if search query hasn't changed within the last 300 ms
         * - If the user is typing fast "Hannes" and than deletes and types "Hannes" again (exceeding 300 ms) the search should not execute twice.
         */

        return Observable.create { emitter ->
            var lastRequest = "" // used to find same requests
            searchView.onSearchTextchanged()
                    .filter { it.length > 2 } // length filter
                    .debounce(300, TimeUnit.MILLISECONDS) // we will send request only if search query hasn't changed within the last 300 ms
                    .filter { it != lastRequest } // filter same requests
                    .map { searchString ->
                        lastRequest = searchString // replace previous search string with current one
                        personBackend.searchfor(searchString)
                                .filter { !it.isEmpty() } // filtering empty results
                                .subscribe { emitter.onNext(it) } // MAGIC!
                    }
                    .doOnComplete { emitter.onComplete() }
                    .subscribeOn(Schedulers.computation())
                    .subscribe()
        }
    }
}
