package person.favorites

import person.types.PersonWithAddress
import io.reactivex.Observable

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {

        /**
         * Provide an observable that only emits a list of PersonWithAddress if they are marked as favorite ones.
         */

        return Observable.create { emitter ->
            favoritesDatabase.favoriteContacts().map { ids ->
                val favoritesPersons = mutableListOf<PersonWithAddress>()
                personBackend.loadAllPersons().map { persons ->
                    persons.forEach {
                        if(ids.contains(it.person.id)) {
                            favoritesPersons.add(it)
                        }
                    }
                }.subscribe()
                emitter.onNext(favoritesPersons)
            }.doOnComplete { emitter.onComplete() }.subscribe()
        }
    }
}
