package fibonacci;

import io.reactivex.Observable;

public class FibonacciExercises {

	public Observable<Integer> fibonacci(int n) {
		return Observable.create(e -> {
			e.onNext(0);

			for(int i = 1; i < n; i++) {
				int a = 0;
				int b = 1;
				for (int j = 2; j <= i; ++j) {
					int next = a + b;
					a = b;
					b = next;
				}
				e.onNext(b);
			}

			e.onComplete();
		});
	}
}
